package happyfamily;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
//    Pet bunny= new Pet();
//    bunny.nickname= "Зая";
//    bunny.species= "Кролик";
//    bunny.trickLevel= 75;
//    bunny.age=2;
//    bunny.habits= new String[]{"Есть моркуву","Есть капусту"};
//
//    Human katia= new Human();
//    katia.name= "Екатерина";
//    katia.surname= "Пупкина";
//    Human oleg= new Human();
//    oleg.name= "Олег";
//    oleg.surname= "Пупкин";
//
//    Human vasia=new Human();
//    vasia.name= "Вася";
//    vasia.surname= "Пупкин";
//    vasia.year=1977;
//    vasia.iq=80;
//    vasia.pet=bunny;
//    vasia.mother=katia;
//    vasia.father=oleg;
//
//
//    Pet dog= new Pet("Пес","Гав");
//    dog.trickLevel= 75;
//    dog.age=2;
//    dog.habits= new String[]{"Гавкать","Приносить тапки"};
//    Human rita= new Human("Маргарита","Бабочкина",1953);
//    Human evgen= new Human("Евгений","Бабочкин",1950);
//    Human kolia= new Human("Николай","Бабочкин",1981);
//    kolia.mother=rita;
//    kolia.father=evgen;
//    kolia.pet=dog;
//    kolia.iq=99;
//    System.out.println(kolia.toString());
//
//    Pet cat1= new Pet("Кот","Кот",6,100,new String[]{"есть","Есть"});

//    Human valera= new Human("Валера","Дудкин",1965);
//    Human ira= new Human("Ира","Дудкина",1991, natasha, valera);
//    ira.iq=60;
//    ira.pet=cat1;
//    System.out.println(ira.toString());

    Man victor= new Man("Виктор","Поп",1955, 67, new HashMap<String, String>() {{put(DayOfWeek.MONDAY.getTranslation(),"Кино, Диван");}});
    Women elena= new Women("Елена","Поп",1963, 68, new HashMap<String, String>() {{put(DayOfWeek.THURSDAY.getTranslation(),"Кино, Диван");}});
    Women alla= new Women("Алла","Поп",1986,70,new HashMap<String, String>() {{put(DayOfWeek.SUNDAY.getTranslation(),"Кино, Диван");}});
    Women natasha=new Women("Наташа","Дудкина",1968,80,new HashMap<String, String>() {{put(DayOfWeek.SATURDAY.getTranslation(),"Кино, Диван");}});
    Man kolia= new Man("Николай","Бабочкин",1981,99,new HashMap<String, String>() {{put(DayOfWeek.MONDAY.getTranslation(),"Кино, Диван");}});
    Man taras=new Man();



    System.out.println(alla);

    Family pop= new Family(elena,victor);
    DomesticCat cat= new DomesticCat("Мурка",4,80, new HashSet<String>(){{
        add("Спать");
        add("Жрать");
    }});
    pop.setPet(new HashSet<Pet>(){{add(cat);}});
    pop.addChild(alla);
    pop.addChild(natasha);
    pop.addChild(kolia);
    elena.greetPet();
    victor.greetPet();
    victor.repairCar();
    elena.makeUp();

        System.out.println(pop);
        pop.deleteChild(natasha);
        pop.deleteChild(victor);
        System.out.println(pop);
        System.out.println(natasha);
        System.out.println(pop.countFamily());


        pop.getChildren();

        System.out.println(pop.hashCode());

        cat.foul();
        cat.respond();

        alla.describePet();
        alla.feedPet(false);


        CollectionFamilyDao families = new CollectionFamilyDao();
        System.out.println(families.getAllFamilies());
        families.saveFamily(pop);
        System.out.println(families.getAllFamilies());
        System.out.println(families.deleteFamily(0));
        System.out.println(families.getAllFamilies());
        System.out.println(families.deleteFamily(0));
        System.out.println(families.deleteFamily(pop));
        Family nk= new Family(natasha,kolia);
        families.saveFamily(nk);
        families.saveFamily(pop);
        System.out.println(families.getAllFamilies());
        nk= new Family(alla,kolia);
        families.saveFamily(nk);
        System.out.println(families.getAllFamilies());
        System.out.println(families.getFamilyByIndex(15));
        System.out.println(families.getFamilyByIndex(1));
        families.deleteFamily(nk);
        System.out.println(families.getAllFamilies());

//        for (int i = 0; i <100000; i++) {
//            Human human = new Human();
//        }

    }
}
